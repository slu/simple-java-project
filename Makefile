.PHONY: build run prepare clean

SHELL := /bin/bash

CLASSPATH  = commons-cli-1.6.0/commons-cli-1.6.0.jar
SOURCE_DIR = source
TARGET_DIR = target
MAIN_CLASS = demo.HelloWorld

MAIN_CLASS_FILE := $(TARGET_DIR)/$(subst .,/,$(MAIN_CLASS)).class
SOURCE_FILES    := $(wildcard $(SOURCE_DIR)/**/*.java)

classpathify = $(subst $(eval ) ,:,$(wildcard $1))


# Targets for compiling ################################################

build: $(MAIN_CLASS_FILE)

$(MAIN_CLASS_FILE): $(SOURCE_FILES)
	CLASSPATH=$(CLASSPATH) \
	javac -d $(TARGET_DIR) $<


# Target for running program ###########################################

run: build
	java -cp $(call classpathify,$(TARGET_DIR) $(CLASSPATH)) $(MAIN_CLASS)


# Target for preparing build ###########################################

prepare:

ifneq (, $(shell command -v curl))
	curl -LO $(URL_BASE)$(ZIP_NAME)
else ifneq (, $(shell command -v wget))
	wget $(URL_BASE)$(ZIP_NAME) -O $(ZIP_NAME)
else
	$(error Cannot find neither curl nor wget commands in PATH)
endif
	jar --extract --verbose --file $(ZIP_NAME) $(CLASSPATH)

prepare: URL_BASE = https://archive.apache.org/dist/commons/cli/binaries/
prepare: ZIP_NAME = commons-cli-1.6.0-bin.zip


# Target deleting generated files ######################################

clean:
	rm -fR $(TARGET_DIR)
