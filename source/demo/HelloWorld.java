package demo;

import org.apache.commons.cli.*;

class HelloWorld {
    private static Options buildOptions() {
        Option help = new Option("help", "display this message");        
        Options options = new Options();
        options.addOption(help);

        return options;
    }
    
    private static CommandLine parse(String[] args, Options options) {
        CommandLineParser parser = new DefaultParser();
        CommandLine line = null;
        
        try {
            line = parser.parse(options, args);
        } catch (ParseException exp) {
            System.err.println("Parsing failed.  Reason: " + exp.getMessage());
        }

        return line;
    }
    
    public static void main(String[] args) {
        Options options = buildOptions();
        CommandLine line = parse(args, options);

        if (line == null) System.exit(1);

        if (line.hasOption("help")) {
            HelpFormatter formatter = new HelpFormatter();
            formatter.printHelp("HelloWorld", options);
        } else {
            System.out.println("Hello, World!");
        }
    }
}
