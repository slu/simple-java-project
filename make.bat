@setlocal
@echo off
set scripts_dir=scripts
set default_command=build
set command=%default_command%

if not "%1" == "" set command=%1
if "%1" == "/h" set command=help

set script=%scripts_dir%\%command%.bat

if not exist %script% (
   if not "%command%" == "help" (
      echo Unknown command: %command%
      echo.
   )
   echo Available commands are:
   echo.
   echo   help - displays this help
   for /f %%s in ('dir scripts /b ^| findstr /b /v _') do @%scripts_dir%\%%s /h
   echo.
   echo Default command is: %default_command%
   goto:eof
)   

echo on
@call %script%
