@if "%1" == "/h" echo   %~n0 - compiles all Java files && goto:eof
@call %~dp0_setvars.bat
set sources=sources.txt
dir %SOURCE_DIR% /b /s | findstr /e .java >%sources%
javac -d %TARGET_DIR% @%sources%
