@if "%1" == "/h" echo   %~n0 - download requried jar && goto:eof
@call %~dp0_setvars.bat

set url_base=https://dlcdn.apache.org/commons/cli/binaries/
set zip_name=commons-cli-1.6.0-bin.zip
set jar_name=%zip_name:-bin.zip=.jar%
set dir_name=%jar_name:.jar=%

PowerShell -Command "& {Invoke-WebRequest %url_base%%zip_name% -OutFile %zip_name%}"
jar --extract --verbose --file %zip_name% %dir_name%/%jar_name%

