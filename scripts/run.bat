@if "%1" == "/h" echo   %~n0 - runs the program && goto:eof
@call %~dp0_setvars.bat
@if not exist %TARGET_DIR%\%MAIN_CLASS:.=\%.class @call make build
set cp=%TARGET_DIR%
if not "%CLASSPATH%" == "" set cp=%cp%;%CLASSPATH%
java -cp %cp% %MAIN_CLASS%
